package com.fooapp.echo.gateway.http;

import com.fooapp.echo.conf.log.LogKey;
import com.fooapp.echo.domain.Echo;
import com.fooapp.echo.gateway.http.jsons.EchoRequest;
import com.fooapp.echo.gateway.http.jsons.EchoResponse;
import com.fooapp.echo.usecase.ExecuteEcho;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import static net.logstash.logback.argument.StructuredArguments.value;

@Slf4j
@RestController
@RequestMapping("/")
@Api(value = "/", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class HelloController {

  private ExecuteEcho executeEcho;

  @Autowired
  public HelloController(ExecuteEcho executeEcho) {
    this.executeEcho = executeEcho;
  }

  @ApiOperation(value = "Hello request")
  @ApiResponses(
          value = {
                  @ApiResponse(code = 200, message = "Hello done successfully"),
                  @ApiResponse(code = 408, message = "Request Timeout"),
                  @ApiResponse(code = 500, message = "Internal Server Error")
          }
  )
  @RequestMapping(method = RequestMethod.GET)
  @ResponseStatus(HttpStatus.OK)
  public String hello() {
    log.info("Requesting an Hello");
    return "Hello, form the app";
  }
}
